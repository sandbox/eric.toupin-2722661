-- SUMMARY --

The Field Group Roles module adds very basic form field access control using Field Groups
configured to include role names in their machine name. When creating new Field Groups for node
or other form types, setting the machine name for the Field Group according to the Field Group Roles
naming convention allows the creation of groups that only render for a specific user role. The
naming convention for Field Group Roles groups is as follows:

group_fgr_[name_of_role]

Role names should use underscores instead of spaces, and should use lowercase letters. For
example, the role name "Student Account" would be "student_account", and a Field Group that ONLY renders
for the "Student Account" role should have the machine name: group_fgr_student_account

For a full description of the module, visit the project page:
http://drupal.org/project/field_group_roles

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/field_group_roles


-- REQUIREMENTS --

Field Groups.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
* There are no configuration or permissions options. All of this module's functionality
is active as soon as the module is enabled.

-- CONFIGURATION --

Field Group Roles relies on form level Field Group configuration for its functionality. 
Fiel Groups can be configured to render ONLY for users with specific user roles by properly
configuring the Field Group's machine name.

When creating new Field Groups for node or other form types, setting the machine name for the 
Field Group according to the Field Group Roles naming convention allows the creation of
groups that only render for a specific user role. The naming convention for Field Group
Roles groups is as follows:

group_fgr_[name_of_role]

Role names should use underscores instead of spaces, and should use lowercase letters. For
example, the role name "Student Account" would be "student_account", and a Field Group that ONLY renders
for the "Student Account" role should have the machine name: group_fgr_student_account

-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - http://drupal.org/user/54136
* Peter Wolanin (pwolanin) - http://drupal.org/user/49851
* Stefan M. Kudwien (smk-ka) - http://drupal.org/user/48898
* Dave Reid (Dave Reid) - http://drupal.org/user/53892